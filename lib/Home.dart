import 'package:flutter/material.dart';
import 'package:kalkulator_barisan_geometri/Barisangeometri.dart';
import 'package:kalkulator_barisan_geometri/Pangkat.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        title: Text("MENU UTAMA"),
        centerTitle: true,
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.only(left: 40, right: 40),
          child: ListView(
            children: [
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Barisangeometri()));
                  },
                  child: Text(
                    "Kalkulator Barisan Geometri",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blueAccent),
              RaisedButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Pangkat()));
                  },
                  child: Text(
                    "Kalkulator Perpangkatan",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blueAccent),
            ],
          ),
        ),
      ),
    );
  }
}
