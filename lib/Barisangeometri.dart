import 'dart:math';
import 'package:flutter/material.dart';

class Barisangeometri extends StatefulWidget {
  @override
  _BarisangeometriState createState() => _BarisangeometriState();
}

class _BarisangeometriState extends State<Barisangeometri> {
  TextEditingController nilaiNController = TextEditingController();
  TextEditingController nilaiAController = TextEditingController();
  TextEditingController nilaiRController = TextEditingController();
  double hasil = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Cari Nilai Un"),
          backgroundColor: Colors.blueAccent,
        ),
        body: ListView(
          children: [
            SizedBox(
              height: 70,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Text("Nilai n"),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: TextFormField(
                controller: nilaiNController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    hintText: "Masukan nilai suku ke - n",
                    border: OutlineInputBorder()),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Text("Nilai a"),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: TextFormField(
                controller: nilaiAController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    hintText: "Masukan nilai awal (a)",
                    border: OutlineInputBorder()),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Text("Nilai r"),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: TextFormField(
                controller: nilaiRController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    hintText: "Masukan nilai Rasio (r)",
                    border: OutlineInputBorder()),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: RaisedButton(
                onPressed: () {
                  (nilaiNController.text != "" &&
                          nilaiAController.text != "" &&
                          nilaiRController.text != "")
                      ? hitungbarisan(
                          double.parse(nilaiNController.text),
                          double.parse(nilaiAController.text),
                          double.parse(nilaiRController.text))
                      : null;
                },
                child: Text(
                  "Hasil",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blueAccent,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Center(child: Text(hasil.toString())),
            ),
          ],
        ));
  }

  hitungbarisan(double n, double a, double r) {
    double pangkat = n - 1;
    double hasilpangkat = pow(r, pangkat);
    print(hasilpangkat);
    setState(() {
      hasil = a * hasilpangkat;
    });
  }
}
