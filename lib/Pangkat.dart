import 'dart:math';

import 'package:flutter/material.dart';

class Pangkat extends StatefulWidget {
  @override
  _PangkatState createState() => _PangkatState();
}

class _PangkatState extends State<Pangkat> {
  TextEditingController inputanNilai =
      TextEditingController(); // inisialisasi controller inputan
  TextEditingController inputanPangkat =
      TextEditingController(); // inisialisasi controller inputan

  int hasil = 0; //inisialsasi variabel global

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.blueAccent,
          title: Text("Kalkulator Perpangkatan"),
          centerTitle: true),
      body: ListView(
        padding: EdgeInsets.all(30),
        children: [
          SizedBox(height: 20),
          TextField(
            controller: inputanNilai,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                hintText: "Masukkan bilangan pokok",
                border: OutlineInputBorder()),
          ),
          SizedBox(height: 20),
          TextField(
            controller: inputanPangkat,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                hintText: "Masukkan bilangan pangkat",
                border: OutlineInputBorder()),
          ),
          SizedBox(height: 20),
          RaisedButton(
            onPressed: () {
              hitungPangkat();
            },
            child: SizedBox(
              height: 60,
              child: Center(
                child: Text(
                  "Tampilkan Hasil",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
            color: Colors.blueAccent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(100),
            ),
          ),
          SizedBox(height: 40),
          Container(
            child: Text(
              hasil
                  .toString(), //fungsi .toString -> Convert from integer to String
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 40,
              ),
            ),
          ),
        ],
      ),
    );
  }

  // void hitungPangkat() -> Method, gunakan nama yang sesuai dengan aksi yg diinginkan
  // 2 ^ 3 = 8 -> ini rumusnya
  // int -> singaktan dari integer, type data
  // nilai -> nama variabel
  // = 5 -> isi dari variabel
  // hasil = nilai ^ pangkat; -> ini koding
  // print(hasil); -> gunanya untuk menampilkan hasil di console

  void hitungPangkat() {
    // //1. Menampilkan di console
    // int nilai = 10;
    // int pangkat = 2;
    // int hasil = 0;
    // hasil = pow(nilai, pangkat);
    // print("---> " + hasil.toString());

    // //- - - - - -- - - - - -
    // //2. Menampilkan di Form
    // setState(() {
    //   int nilai = 7;
    //   int pangkat = 2;

    //   hasil = pow(nilai, pangkat);
    // });

    //- - - - - -- - - - - -
    //3. Menampilkan di Form menggunakan inputan
    setState(() {
      int nilai = int.parse(inputanNilai.text);
      int pangkat = int.parse(inputanPangkat.text);

      hasil = pow(nilai, pangkat);
    });
  }
}
